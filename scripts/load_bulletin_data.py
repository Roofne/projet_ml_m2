import random
import pandas as pd
import glob
import os

from scripts.CustomLogger import CustomLogger
from datetime import datetime

from apps.meteo.models import Station, Bulletin

def run():
    logger = CustomLogger()
    
    # on liste les fichiers csv des bulletins
    folder_path = "data/bulletin/"
    csv_files   = glob.glob(os.path.join(folder_path, '*.csv'))
    
    for file in csv_files: #random.choices(csv_files, k=20):    
        
        # on charge un dataset contenant les bulletins du mois
        df = pd.read_csv(file, sep=';')
        df.replace("mq", None, inplace=True)
            
        # pour chaque bulletin du dataset
        for idx,bulletin in df.iterrows():
            
            # on parse les dates
            date_value  = str(bulletin['date'])
            date_format = "%Y%m%d%H%M%S"
            date = datetime.strptime(date_value, date_format)
            
            # on parse les températures
            if bulletin['t_neige']:
                temp = round(float(bulletin['t_neige']) - 273.15,2)
            else:
                temp = None
                
            if bulletin['td']:
                temp_rosee = round(float(bulletin['td']) - 273.15,2)
            else:
                temp_rosee = None               
            
            try:
                station_id = bulletin['numer_sta']
                station,is_created = Station.objects.get_or_create(id=station_id)
                
                args = {
                    "date"    : date, 
                    "temp"    : temp,
                    "temp_rosee" : temp_rosee,
                    "p_humidite" : bulletin['u'],
                    "h_neige" : bulletin['ht_neige_alti'],
                    "h_neige_fraiche" : bulletin['neige_fraiche'],
                    "v_vent"     : bulletin['ff'],
                    "dir_vent"   : bulletin['dd'],
                    "nebulosite_totale" : bulletin['n'],
                    "nebulosite_nuage"  : bulletin['nbas'],
                    "h_nuage" : bulletin['hbas'],
                    "station" : station,
                }
                                
                # on creer une entrée
                obj = Bulletin.objects.create(**args)
                logger.info("BULLETIN DU %s A %s CREATED" % (str(date), str(station)))
            except Station.DoesNotExist:
                logger.warning("Station with id %s not found" % (str(station_id)))
        
        print("Bulletin data from "+str(file)+" imported")
                
                