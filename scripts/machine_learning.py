
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.discriminant_analysis import StandardScaler

import pandas as pd

from apps.meteo.models import Bulletin

def test_model(data, features, target):
    
    #transformer d'abord data en numpy avec la méthode to_numpy
    dataNp = data.to_numpy()
    
    #séparation des données explicatives X et de la cible Y avec le slicing
    X = dataNp[:,0:-2] # variables expliquatives
    Y = dataNp[:,-1]    # variable à prédire
    
    # Normaliser les donnes
    X_normalized = StandardScaler().fit_transform(X)
    X_scaled     = MinMaxScaler().fit_transform(X)
    X_choice     = [X,X_scaled,X_normalized]

    # on les entraines et on les test
    X_train, X_test, Y_train, Y_test = train_test_split(X_choice[0], Y, train_size=0.7)
    
    # Modèle Regression linéaire
    model = LinearRegression()
    model.fit(X_train, Y_train)
    Y_pred = model.predict(X_test)
    
    # on calcule les scores
    s1 = model.score(X_test, Y_test)    
    
    print("Linear Regression model Score with %s => %s : %f " % (features,target,s1))    


def run():
     # on selectionne les variables d'entrées et de sorties
    features = ["temp","temp_rosee","p_humidite","v_vent","dir_vent","nebulosite_totale","nebulosite_nuage","h_nuage","h_neige_fraiche"]
    features = ["temp","p_humidite","v_vent","nebulosite_totale","h_nuage","h_neige_fraiche"]

    # on recupère les données de la BDD
    data = Bulletin.objects.values(*features)
    
    # on recupere la variable objectif
    target = features.pop()
        
    # on transforme le QuerySet en DataSet
    data = list(data)
    df   = pd.DataFrame(data)        
    df   = df.dropna() # on enlèves les lignes contenant des données vides
    
    # on prépare test un model avec ces variables
    test_model(df, features, target)

    
    




# SOURCES

# https://moncoachdata.com/blog/regression-lineaire-sur-un-cas-reel/
# https://towardsdatascience.com/how-to-improve-the-accuracy-of-a-regression-model-3517accf8604