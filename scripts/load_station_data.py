from apps.meteo.models import Station
from scripts.CustomLogger import CustomLogger
import pandas as pd

def run():
    logger = CustomLogger()

    # chargement du dataset
    df = pd.read_csv('./data/station/postesNivo.csv')
    
    # pour chaque station du dataset
    for idx,station in df.iterrows():

        # on parse les données
        id = int(station['ID'])
        args = {
            "id"  : id,
            "nom" : station['Nom'].upper(),
            "latitude"  : float(station['Latitude']),
            "longitude" : float(station['Longitude']),
            "altitude"  : int(station['Altitude']),
        }
            
        # on creer une entre
        try:
            obj = Station.objects.get(id=id)
            obj.update(**args)
            logger.info("STATION UPDATED : %s" % str(obj)) 
        except Station.DoesNotExist:
            logger.info("STATION NOT FOUND : %s" % str(station['Nom'])) 
        
    print("Station data imported")


