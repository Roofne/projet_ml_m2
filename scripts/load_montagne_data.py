from sklearn.model_selection import train_test_split
from sklearn.cluster import KMeans
from sklearn import preprocessing

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

from apps.meteo.models import Station, Montagne

def run():
    # on creer des montagnes dans la BDD
    m1 = Montagne.objects.create(**{"nom":"MONTAGNE 1"})
    m2 = Montagne.objects.create(**{"nom":"MONTAGNE 2"})
    m3 = Montagne.objects.create(**{"nom":"MONTAGNE 3"})
    m4 = Montagne.objects.create(**{"nom":"MONTAGNE 4"})
    montagnes = [m1,m2,m3,m4]
    
    # on creer un dataset de station
    stations = Station.objects.all().values_list('id','latitude','longitude').exclude(latitude__isnull=True)
    stations = list(stations)
    X = pd.DataFrame(stations, columns=['id','latitude','longitude'])
    
    # on visualize les données
    sns.scatterplot(data=X, x='longitude', y='latitude')
    
    # on utilise l'algorithem K-Means pour generer 4 cluster de stations
    X_train, X_test, y_train, y_test = train_test_split(X[['latitude', 'longitude']], X[['id']], test_size=0.33, random_state=0)
    X_train_norm = preprocessing.normalize(X_train)
    X_test_norm  = preprocessing.normalize(X_test)

    # Get cluster labels and cluster centers
    kmeans = KMeans(n_clusters = 4, random_state = 0, n_init='auto')
    kmeans.fit(X_train_norm)
        
    # on visualize les clusters
    sns.scatterplot(data = X_train, x='longitude', y='latitude', hue = kmeans.labels_)
    
    # on associe une montagne à chaque station
    for i in range(0,len(stations)):
        station_id = stations[i]
        station = Station.objects.get(id=station_id)
        montagne_id = kmeans.labels_[i]
        station.montagne = montagnes[montagne_id]
        station.save()
    
    



        