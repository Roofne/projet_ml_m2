# PROJET_ML_M2

Ce projet est une  application web destiné à la visualisation et a la prediction des chutes de neiges
dans les stations de ski de France.
Les données ont été récupéré sur le site de Météo France.

# REQUIREMENT
```
Python 3.13
Django 
```

# INSTALATION

```
git clone repo
install.bat
migrate.bat
```