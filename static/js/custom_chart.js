function sendRequest(url, successCallback) {
    fetch(url)  // on recupère les données depuis le backend
    .then(response => {
        if (!response.ok) { throw new Error('Network response was not ok'); }
        return response.json(); // Parse JSON data
    })
    .then(data => {
        successCallback(data)
    })
    .catch(error => {
        console.log("Error when calling : "+url)
    });
}

function draw_line_chart(data, id, x, y) {
    console.log(data)
    var ctx = document.getElementById(id).getContext('2d');
    var chart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: data.map(item => item[x]),  // Extract x values from data
            datasets: [{
                data: data.map(item => item[y]),  // Extract y values from data
                borderColor: '#0d6efd',
                borderWidth: 1,
                fill: false,
            }]
        },
        options: {
            scales: {
                x: {
                    type: 'time', // Specify time scale for x-axis
                    time: {
                        unit:'year',
                    }
                },
            },
            /*
            ADD BOX FOR EACH YEAR
            plugins: {
                annotation: {
                    drawTime: 'afterDatasetsDraw',
                    annotations: [{
                        id: 'year-1',
                        type: 'box',
                        xScaleID: 'x',
                        xMin: dayjs('2023-11-01').format('YYYY-MM-DD'),
                        xMax: dayjs('2024-05-01').format('YYYY-MM-DD'),
                        backgroundColor: 'rgba(255, 0, 0, 0.2)',
                    },{
                        id: 'year-2',
                        type: 'box',
                        xScaleID: 'x',
                        xMin: dayjs('2022-11-01').format('YYYY-MM-DD'),
                        xMax: dayjs('2023-05-01').format('YYYY-MM-DD'),
                        backgroundColor: 'rgba(0, 255, 0, 0.2)',
                    }]                    
                }
            }
            */
        }
    });    
}

function draw_bar_chart(data) {
 
}

function draw_pie_chart(data) {
    
}