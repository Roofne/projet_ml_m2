from typing import Any
from datetime import date

from django.forms import FloatField
from django.shortcuts import render
from django.template.response import TemplateResponse
from django.views.generic import DetailView, ListView, TemplateView
from django.http import HttpResponse, JsonResponse
from django.db.models import Min, Max, Avg, Count, Func, Q
from django.db.models.functions import ExtractYear


from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.discriminant_analysis import StandardScaler

from apps.meteo.models import Station, Bulletin

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# CUSTOM CLASSES

class Round(Func):
  function = 'ROUND'
  arity = 2  

# FUNCTION VIEWS

def get_stations_ids():
    data = Station.objects.exclude(nom=None).values('id')
    data = [item['id'] for item in data.values('id')]
    return data

def carte_json(request):
    data = Station.objects.exclude(nom=None).values('nom','latitude','longitude')
    json = [{'x': item['nom'], 'y': item['latitude'], 'z': item['longitude']} for item in data]
    return JsonResponse(json, safe=False)

def generate_matrix_correlation():
     # on convertit les entrées de la BDD en dataset
    data = Bulletin.objects.values("temp","temp_rosee","p_humidite","h_neige","h_neige_fraiche","v_vent","dir_vent","nebulosite_totale","nebulosite_nuage","h_nuage")
    df   = pd.DataFrame(data)
    
    # on genere la matrice de correlation    
    correlation_matrix = df.corr()
    
    # on trace la matrice
    plt.figure(figsize=(10, 10))
    sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', fmt=".2f")
    plt.title('Correlation Matrix')
    plt.tight_layout()
    
    # on sauvegarde la matrice
    filename = 'matrix_correlation.png'
    plt.savefig("./static/img/figure/"+filename)
    plt.close()
    
def temperature_min_per_station(station_id):
    temp = Bulletin.objects.filter(station=station_id).values('date','temp')
    df   = pd.DataFrame(temp)
    df['date'] = pd.to_datetime(df.date, format='%Y-%m-%d')      
    temp = df.groupby(df['date'].dt.year)['temp'].min()
    temp = [ {'date':key,'temp':value} for key,value in temp.items() ]
    return { station_id : temp }

def get_stations():
    return Station.objects.exclude(nom=None)

def test(request):   
    return HttpResponse(1)    

# JSON REQUEST

def handle_request(request, json):
    if request:
        return JsonResponse(json, safe=False)
    else:
        return json

def station_json(request):
    '''
    Renvoie un JSON contenant les données des stations
    
    Args:
        request (HttpRequest) : une requète HTTP (peut etre null)
        
    Returns:
        JsonResponse : une réponse JSON
    '''
    json = Station.objects.exclude(nom=None)
    json = json.order_by(request.GET.get('order_by')) if request.GET.get('order_by') else json
    json = list(json.values())
    return JsonResponse(json, safe=False)

def station_by_id_json(request, pk):
    json = Station.objects.exclude(nom=None).filter(id=pk)
    json = list(json.values())
    return JsonResponse(json, safe=False)

def station_by_id_bulletin_json(request, pk):
    json = Bulletin.objects.filter(station=pk)
    json = list(json.values())
    return JsonResponse(json, safe=False)

def station_by_id_temp_stat_json(request, pk):
    bulletins = Bulletin.objects.filter(station=pk)
    json = bulletins.annotate(
         year=ExtractYear('date')
    ).values('year').annotate(
        count = Count('temp'),
        min   = Min('temp'),
        avg   = Round(Avg('temp'), 2.0),
        max   = Max('temp'),
    ).order_by('year')
            
    # on renvoie des données JSON
    json = list(json)
    return JsonResponse(json, safe=False)

def station_by_id_temp_json(request, pk):
    if request.GET.get('summary'):
        return station_by_id_temp_stat_json(request, pk)
    
    json = Bulletin.objects.filter(station=pk)
    json = json.values('date','temp').exclude(temp__isnull=True).order_by('date')
    
    # on filtre par date
    start_date = request.GET.get('start_date')
    end_date   = request.GET.get('end_date')
    json = json.filter(date__range=(start_date,end_date)) if start_date and end_date else json
    
    # on renvoie des données JSON
    json = list(json)
    return JsonResponse(json, safe=False)


def station_by_id_neige_json(request, pk):
    # on récupère les dates et les hauteurs de neiges
    json = Bulletin.objects.filter(station=pk)
    json = json.values('date','h_neige','h_neige_fraiche').exclude(h_neige__isnull=True).order_by('date')
    
    print(json)
    
    # on filtre par date
    start_date = request.GET.get('start_date')
    end_date   = request.GET.get('end_date')
    json = json.filter(date__range=(start_date,end_date)) if start_date and end_date else json
    
    # on renvoie des données JSON
    json = list(json)
    return JsonResponse(json, safe=False)


# CLASS VIEWS

class IndexView(TemplateView):
    template_name = "index.html"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["stations"] = get_stations()[:25]
        return context

def train_model(data, features, target):
    
    #transformer d'abord data en numpy avec la méthode to_numpy
    dataNp = data.to_numpy()
    
    #séparation des données explicatives X et de la cible Y avec le slicing
    X = dataNp[:,0:-1] # variables expliquatives
    Y = dataNp[:,-1]    # variable à prédire
    
    # Normaliser les donnes
    X_normalized = StandardScaler().fit_transform(X)
    X_scaled     = MinMaxScaler().fit_transform(X)
    X_choice     = [X,X_scaled,X_normalized]

    # on les entraines et on les test
    X_train, X_test, Y_train, Y_test = train_test_split(X_choice[0], Y, train_size=0.7)
    
    # Modèle Regression linéaire
    model = LinearRegression()
    model.fit(X_train, Y_train)
    Y_pred = model.predict(X_test)
    
    # on calcule les scores
    s1 = model.score(X_test, Y_test)    
    print("Linear Regression model Score with %s => %s : %f " % (features,target,s1))    
    
    return model


class PrevisionView(TemplateView):
    template_name = "prevision.html"
    
    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        print("INIT FORM MACHINE LEARNING")
        return self.render_to_response(context)
    
    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        print("HANDLING FORM MACHINE LEARNING")
        
        print(request)
        
        # on selectionne les variables d'entrées et de sorties
        features = ["temp","temp_rosee","p_humidite","v_vent","dir_vent","nebulosite_totale","nebulosite_nuage","h_nuage","h_neige_fraiche"]
        features = ["temp","p_humidite","v_vent","nebulosite_totale","h_nuage","h_neige_fraiche"]

        # on recupère les données de la BDD
        data = Bulletin.objects.values(*features)
        
        # on recupere la variable objectif
        target = features.pop()
            
        # on transforme le QuerySet en DataSet
        data = list(data)
        df   = pd.DataFrame(data)        
        df   = df.dropna() # on enlèves les lignes contenant des données vides
        
        # on prépare test un model avec ces variables
        model = train_model(df, features, target)
        
        # parsing request value
        temp = int(request.POST.get('temp'))
        p_humidite = int(request.POST.get('p_humidite'))
        v_vent = int(request.POST.get('v_vent'))
        nebulosite_totale = int(request.POST.get('nebulosite_totale'))
        h_nuage = int(request.POST.get('h_nuage'))
        values = [temp,p_humidite,v_vent,nebulosite_totale,h_nuage]
        print(values)
        
        # predict hauteur neige
        values = np.array([values])
        Y_pred = model.predict(values)[0]
        
        predictions = {'intercept': model.intercept_, 'coefficient': model.coef_,   'predicted_value': Y_pred}
        print(predictions)
        
        # ajout de la reponse au context
        context['h_neige'] = round(Y_pred,2)
        
        return self.render_to_response(context)

# VUES DE LA CLASSE STATION

class StationListView(ListView):
    template_name = "station/station-list.html"  
    model = Station 
    ordering = ['nom']
    
    def get_queryset(self):
        return get_stations()

class StationDetailView(DetailView):
    template_name = "station/station-detail.html"
    model = Station
    
# VUES DE LA CLASSE BULLETIN

class BulletinListView(ListView):
    template_name = "bulletin/bulletin-table.html"  
    model = Bulletin
    
class BulletinDetailView(DetailView):
    template_name = "bulletin/bulletin-detail.html"  
    model = Bulletin

class BulletinParStationListView(ListView):
    template_name = "bulletin/bulletin-table.html"  
    model = Bulletin
    
    def get_queryset(self):
        station_id = self.kwargs['pk1']
        queryset = Bulletin.objects.filter(station=station_id)
        return queryset
    
class BulletinParStationGraphView(ListView):
    template_name = "bulletin/bulletin-graph.html"  
    model = Bulletin
    
    def get_queryset(self):
        station_id = self.kwargs['pk1']
        queryset = Bulletin.objects.filter(station=station_id)
        return queryset

# VUES DES WIDGET

class WidgetCarteView(TemplateView):
    template_name = "carte.html"  
    
class WidgetMatrixCorrelationView(TemplateView):
    template_name = "widgets/matrix_correlation.html"
    
    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        generate_matrix_correlation()                
        return context

# VUE DES ARTICLES

class ArticleView(TemplateView):
    template_name = "articles/article-list.html"
    
class ArticleViewByID(TemplateView):
    template_name = "articles/article_1.html"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)    

        temp = []
        for station_id in get_stations_ids():
            temp.append(temperature_min_per_station(station_id))
        context['temp'] = temp 
        
        return context