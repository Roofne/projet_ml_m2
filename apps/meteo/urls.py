from django.urls import path

from .views import *

urlpatterns = [
    path('', IndexView.as_view(), name="index"),
]

# URLS FOR REQUESTING JSON DATA 
urlpatterns += [
    path('test/',                test,          name="test"),
    path('carte_json/',          carte_json,          name="carte_json"),
    path('json/station/',          station_json,       name="station_json"),
    path('json/station/<int:pk>/', station_by_id_json, name="station_by_id_json"),
    path('json/station/<int:pk>/bulletin/',  station_by_id_bulletin_json,  name='station_by_id_bulletin_json'),
    path('json/station/<int:pk>/bulletin/temp/',  station_by_id_temp_json,  name='station_by_id_temp_json'),
    path('json/station/<int:pk>/bulletin/neige/', station_by_id_neige_json, name='station_by_id_neige_json'),
]

# URLS FOR STATION VIEWS
urlpatterns += [
    path('stations/', StationListView.as_view(), name="station_list"),
    path('stations/<int:pk>/', StationDetailView.as_view(), name="station_detail"),
    path('stations/<int:pk1>/bulletins/', BulletinParStationGraphView.as_view(), name="bulletin_par_station")
]

# URLS FOR BULLETINS VIEWS
urlpatterns += [
    path('bulletins/', BulletinListView.as_view(), name="bulletin_list"),
    path('bulletins/<int:pk>/', BulletinDetailView.as_view(), name="bulletin_detail"),
]

# URLS FOR WIDGETS TEST
urlpatterns += [
    path('widgets/carte/', WidgetCarteView.as_view(), name="widget_carte"),
    path('widgets/matrix/correlation/', WidgetMatrixCorrelationView.as_view(),    name="correlation_view"),
]

# URLS FOR ARTICLES
urlpatterns += [
    path('articles/'         , ArticleView.as_view()    , name="articles"),
    path('articles/<int:pk>/', ArticleViewByID.as_view(), name="articles"),
]

# URLS FOR ML
urlpatterns += [
    path('prevision/'          ,PrevisionView.as_view()    , name="prevision"),
]

