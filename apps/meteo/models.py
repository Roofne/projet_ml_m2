from django.db import models

class Montagne(models.Model):

    id  = models.IntegerField(primary_key=True)
    nom = models.CharField(max_length=255)


class Station(models.Model):
    
    id  = models.IntegerField(primary_key=True)
    nom = models.CharField(max_length=255, null=True)
    latitude  = models.FloatField(null=True)
    longitude = models.FloatField(null=True)
    altitude  = models.IntegerField(null=True)
    montagne  = models.ForeignKey(Montagne, on_delete=models.CASCADE, null=True)
    
    def update(self, **args):
        self.id  = args['id']  if'id' in args.keys() else self.id
        self.nom = args['nom'] if'id' in args.keys() else self.nom
        self.latitude  = args['latitude']  if'latitude'  in args.keys() else self.latitude
        self.longitude = args['longitude'] if'longitude' in args.keys() else self.longitude
        self.altitude  = args['altitude']  if'altitude'  in args.keys() else self.altitude
        self.save()

    def __str__(self):
        return str(self.nom)
    
class Bulletin(models.Model):
    
    id    = models.IntegerField(primary_key=True)
    date  = models.DateField()
    temp    = models.FloatField(null=True)
    temp_rosee = models.FloatField(null=True)
    p_humidite   = models.IntegerField(null=True)
    h_neige = models.FloatField(null=True)
    h_neige_fraiche = models.FloatField(null=True)
    v_vent   = models.FloatField(null=True)
    dir_vent = models.IntegerField(null=True)
    nebulosite_totale = models.IntegerField(null=True)
    nebulosite_nuage  = models.FloatField(null=True)
    h_nuage  = models.FloatField(null=True)
    station  = models.ForeignKey(Station, on_delete=models.CASCADE)
    
    def __str__(self):
        return "Bulletin du "+str(self.date)+" à "+str(Station)
    
    